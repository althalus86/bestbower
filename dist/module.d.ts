declare module Obg.Packages.BannerSwiper {
    var ModuleName: string;
}
declare module Obg.Packages.BannerSwiper {
    var ngModule: ng.IModule;
}
declare module Obg.Packages.BannerSwiper {
}
declare module Obg.Packages.BannerSwiper {
    interface IBanner {
        title?: string;
        subTitle?: string;
        siteImage?: string;
        siteSmallImage?: string;
        mobileImage?: string;
        mobileSmallImage?: string;
        bannerLinkTarget?: string;
        bannerLinkOnClick?: string;
        bannerLinkUrl?: string;
        order?: number;
        buttons?: Array<IButtonContentItem>;
    }
    interface IButtonContentItem {
        text?: string;
        url?: string;
        onClick?: string;
    }
}
