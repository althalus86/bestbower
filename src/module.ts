﻿module Obg.Packages.BannerSwiper {
    "use strict";

    // Create the moduel and define its dependencies.
    export var ngModule: ng.IModule = angular.module(BannerSwiper.ModuleName, []);
} 