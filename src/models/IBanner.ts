﻿module Obg.Packages.BannerSwiper {
    "use strict";

    export interface IBanner {
        title?: string;
        subTitle?: string;
        siteImage?: string;
        siteSmallImage?: string;
        mobileImage?: string;
        mobileSmallImage?: string;
        bannerLinkTarget?: string;
        bannerLinkOnClick?: string;
        bannerLinkUrl?: string;
        order?: number;
        buttons?: Array<IButtonContentItem>;
    }

    // TODO: Need to use the one in SDK-Common once bower component created
    export interface IButtonContentItem {
        text?: string;
        url?: string;
        onClick?: string;
    }
}