﻿module Obg.Packages.BannerSwiper {
    "use strict";

    interface BannerSwiperScope extends ng.IScope {
        data: any;
        device: string;
        isMobile: boolean;
    }

    class BannerSwiper implements ng.IDirective {
        static id = "bannerSwiper";

        restrict = "E";
        scope = {
            data: "=?",
            device: "@"
        };
        replace = true;
        templateUrl = "src/views/banner.html";
        compile: (templateElement: angular.IAugmentedJQuery, templateAttributes: angular.IAttributes, transclude: angular.ITranscludeFunction) => {};

        constructor($timeout: ng.ITimeoutService) {
            this.compile = () => {
                return {
                    post: (scope: BannerSwiperScope) => {
                        //Set the mobile flag to false
                        scope.isMobile = false;
                        //Check if isMobile and set the flag to true
                        if (scope.device === "mobile") {
                            scope.isMobile = true;
                        };

                       //Wait for a digest and instantiate the Swiper
                        $timeout(() => {
                            return new Swiper(".swiper-container", {
                                autoplay: 5000,
                                pagination: ".swiper-pagination",
                                paginationHide: false,
                                paginationClickable: true,
                                paginationElement: "span",
                                preventClicks: false,
                                preventClicksPropagation: false
                            });
                        },600);
                    }
                };
            };
        }

        static factory() {
            var directive = ($timeout: ng.ITimeoutService) => {
                return new BannerSwiper($timeout);
            };

            directive.$inject = ["$timeout"];

            return directive;
        }
    }

    ngModule.directive(BannerSwiper.id, BannerSwiper.factory());
}