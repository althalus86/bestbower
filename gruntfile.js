﻿module.exports = function (grunt) {

    grunt.loadNpmTasks("grunt-tsd");
    grunt.loadNpmTasks("grunt-tslint");
    grunt.loadNpmTasks("grunt-typescript");
    grunt.loadNpmTasks("grunt-contrib-clean");
    grunt.loadNpmTasks("grunt-contrib-concat");
    grunt.loadNpmTasks("grunt-angular-templates");

    grunt.initConfig({
        tsd: {
            refresh: {
                options: {
                    command: "reinstall",
                    latest: true,
                    config: "tsd.json"
                }
            }
        },
        tslint: {
            options: {
                configuration: tsLintConfig
            },
            base: {
                src: ["src/**/*.ts"]
            }
        },
        typescript: {
            base: {
                options: {
                    sourceMap: true,
                    module: "amd",
                    target: "es5",
                    declaration: true,
                    references: ["typings/tsd.d.ts"]
                },
                src: ["src/consts.ts", "src/module.ts", "src/**/*.ts"],
                dest: "build/temp/module.js"
            }
        },
        clean: {
            options: {
                force: true
            },
            build: ["build"],
            release: ["dist"]
        },
        concat: {
            dist: {
                options: {
                    sourceMap: true
                },
                files: {
                    "dist/module.min.js": ["build/temp/**/*.js"],
                }
            },
            def: {
                files: {
                    "dist/module.d.ts": ["build/temp/**/*.d.ts"]
                }
            }
        },
        ngtemplates: {
            template: {
                src: "src/views/**/*.html",
                dest: "build/temp/templates.js",
                options: {
                    module: "obg-ng-banner-swiper"
                }
            }
        }
    });

    grunt.registerTask("install", ["tsd:refresh"]);
    grunt.registerTask("dev", ["clean", "ngtemplates:template", "tslint:base", "typescript:base", "concat"]);

};

var tsLintConfig = {
    "rules": {
        "ban": [
            true,
            ["_", "extend"],
            ["_", "isNull"],
            ["_", "isDefined"]
        ],
        "class-name": true,
        "comment-format": [
            false,
            "check-space",
            "check-lowercase"
        ],
        "curly": true,
        "eofline": false,
        "forin": true,
        "indent": [true, 4],
        "interface-name": false,
        "jsdoc-format": true,
        "label-position": true,
        "label-undefined": true,
        "max-line-length": [true, 180],
        "no-arg": true,
        "no-bitwise": true,
        "no-console": [
            true,
            "debug",
            "info",
            "time",
            "timeEnd",
            "trace"
        ],
        "no-construct": true,
        "no-debugger": true,
        "no-duplicate-key": true,
        "no-duplicate-variable": true,
        "no-empty": true,
        "no-eval": true,
        "no-string-literal": false,
        "no-trailing-comma": true,
        "no-trailing-whitespace": true,
        "no-unused-expression": true,
        "no-unused-variable": true,
        "no-unreachable": true,
        "no-use-before-declare": true,
        "one-line": [
            true,
            "check-open-brace",
            "check-catch",
            "check-else",
            "check-whitespace"
        ],
        "quotemark": [true, "double"],
        "radix": true,
        "semicolon": true,
        "triple-equals": [true, "allow-null-check"],
        "typedef": [
            true,
            "callSignature",
            "indexSignature",
            "parameter",
            "propertySignature",
            "variableDeclarator"
        ],
        "typedef-whitespace": [
            true,
            ["callSignature", "noSpace"],
            ["catchClause", "noSpace"],
            ["indexSignature", "space"]
        ],
        "use-strict": [
            true,
            "check-module",
            "check-function"
        ],
        "variable-name": false,
        "whitespace": [
            false,
            "check-branch",
            "check-decl",
            "check-operator",
            "check-separator",
            "check-type"
        ]
    }
};